package auth

import (
	"fmt"
	"net/http"

	"github.com/stretchr/testify/mock"
)

type mockValidator struct {
	mock.Mock
}

func (m *mockValidator) Validate(a authKey) error {
	args := m.Called(a)
	return args.Error(0)
}

type mockHandler struct {
	mock.Mock
}

func (m *mockHandler) Handle(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%v\n", r)
	m.Called(w, r)
}
