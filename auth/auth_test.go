package auth

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestAuthHandler_AuthorizedForwards(t *testing.T) {
	mockedValidator := new(mockValidator)
	mockedValidator.On("Validate", mock.MatchedBy(func(ak authKey) bool { return true })).Return(nil)
	authHandler := NewAuthHandler(mockedValidator)
	reqRecorder := httptest.NewRecorder()
	underlyingHandler := new(mockHandler)
	body := `{"air_temperature":31.5,"timestamp":"2019-10-18T09:30:13.000Z"}`
	req, err := http.NewRequest("POST", "localhost:8080/metrics", strings.NewReader(body))
	req.Header.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
	assert.Nil(t, err)
	underlyingHandler.On("Handle",
		mock.MatchedBy(func(w http.ResponseWriter) bool { return true }),
		mock.MatchedBy(func(r *http.Request) bool { return r.URL.String() == "localhost:8080/metrics" })).
		Run(func(args mock.Arguments) {
			w := args.Get(0).(http.ResponseWriter)
			w.WriteHeader(http.StatusCreated)
			w.Write([]byte(body))
		})
	handler := authHandler.HandleAuth(http.HandlerFunc(underlyingHandler.Handle))

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusCreated, reqRecorder.Code)
	assert.Equal(t, body, reqRecorder.Body.String())
}

func TestAuthHandler_Malformed(t *testing.T) {
	mockedValidator := new(mockValidator)
	mockedValidator.On("Validate", mock.MatchedBy(func(ak authKey) bool { return true })).Return(fmt.Errorf("invalid pass"))
	authHandler := NewAuthHandler(mockedValidator)
	reqRecorder := httptest.NewRecorder()
	underlyingHandler := new(mockHandler)
	body := `{"air_temperature":31.5,"timestamp":"2019-10-18T09:30:13.000Z"}`
	req, err := http.NewRequest("POST", "localhost:8080/metrics", strings.NewReader(body))
	req.Header.Add("Authorization", "Invalid")
	assert.Nil(t, err)
	handler := authHandler.HandleAuth(http.HandlerFunc(underlyingHandler.Handle))

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Equal(t, `{"errors":[{"detail":"Malformed Authorization header."}]}`, reqRecorder.Body.String())
}

func TestAuthHandler_Unauthorized(t *testing.T) {
	mockedValidator := new(mockValidator)
	mockedValidator.On("Validate", mock.MatchedBy(func(ak authKey) bool { return true })).Return(fmt.Errorf("invalid pass"))
	authHandler := NewAuthHandler(mockedValidator)
	reqRecorder := httptest.NewRecorder()
	underlyingHandler := new(mockHandler)
	body := `{"air_temperature":31.5,"timestamp":"2019-10-18T09:30:13.000Z"}`
	req, err := http.NewRequest("POST", "localhost:8080/metrics", strings.NewReader(body))
	req.Header.Add("Authorization", "Basic dXNlcm5hbWU6cGFzc3dvcmQ=")
	assert.Nil(t, err)
	handler := authHandler.HandleAuth(http.HandlerFunc(underlyingHandler.Handle))

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusUnauthorized, reqRecorder.Code)
	assert.Equal(t, `{"errors":[{"detail":"Unauthorized: invalid pass"}]}`, reqRecorder.Body.String())
}
