package auth

import (
	"fmt"
	"net/http"

	"github.com/kovacshuni/smart-ac/errors"
)

const UsernameHeader = "X-Username"

type AuthHandler struct {
	validator AuthValidator
}

func NewAuthHandler(validator AuthValidator) *AuthHandler {
	return &AuthHandler{
		validator: validator,
	}
}

func (h *AuthHandler) HandleAuth(underlyingHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			errors.WriteJsonApiErrorToHTTP("Malformed Authorization header.", http.StatusBadRequest, w)
			return
		}
		err := h.validator.Validate(authKey{key: username, value: password})
		if err != nil {
			msg := fmt.Sprintf("Unauthorized: %v", err)
			errors.WriteJsonApiErrorToHTTP(msg, http.StatusUnauthorized, w)
			return
		}

		r.Header.Set(UsernameHeader, username)
		underlyingHandler.ServeHTTP(w, r)
	})
}
