package auth

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPasswordValidator_Ok(t *testing.T) {
	userDatabase := map[string]Secrets{
		"218111111111": Secrets{ // pass1
			Salt:   []byte{61, 27, 101, 90, 80, 246, 19, 128, 99, 88, 27, 172, 40, 216, 88, 20},
			N:      32768,
			R:      8,
			P:      1,
			Hashed: []byte{249, 58, 65, 182, 160, 125, 44, 10, 215, 119, 232, 151, 114, 108, 226, 145, 220, 166, 31, 6, 249, 13, 32, 162, 4, 69, 163, 150, 127, 194, 24, 233},
		},
	}
	validator := NewPasswordValidator(userDatabase)

	err := validator.Validate(authKey{key: "218111111111", value: "pass1"})

	assert.Nil(t, err)
}

func TestPasswordValidator_Nok(t *testing.T) {
	userDatabase := map[string]Secrets{
		"218111111111": Secrets{ // pass1
			Salt:   []byte{61, 27, 101, 90, 80, 246, 19, 128, 99, 88, 27, 172, 40, 216, 88, 20},
			N:      32768,
			R:      8,
			P:      1,
			Hashed: []byte{249, 58, 65, 182, 160, 125, 44, 10, 215, 119, 232, 151, 114, 108, 226, 145, 220, 166, 31, 6, 249, 13, 32, 162, 4, 69, 163, 150, 127, 194, 24, 233},
		},
	}
	validator := NewPasswordValidator(userDatabase)

	err := validator.Validate(authKey{key: "218111111111", value: "invalidpass1"})

	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "Invalid credentials")
}
