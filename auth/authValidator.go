package auth

import (
	"fmt"
	"reflect"

	"golang.org/x/crypto/scrypt"
)

const hashLength = 32

type authKey struct {
	key   string
	value string
}

type Secrets struct {
	Salt   []byte
	N      int
	R      int
	P      int
	Hashed []byte
}

type AuthValidator interface {
	Validate(a authKey) error
}

type passwordValidator struct {
	userDatabase map[string]Secrets
}

func NewPasswordValidator(userDatabase map[string]Secrets) *passwordValidator {
	return &passwordValidator{
		userDatabase: userDatabase,
	}
}

/*
 * Using scrypt https://en.wikipedia.org/wiki/Scrypt for hashing and validation
 */
func (v *passwordValidator) Validate(a authKey) error {
	userSecrets, ok := v.userDatabase[a.key]
	if !ok {
		return fmt.Errorf("username=%s not found", a.key)
	}

	dk, err := scrypt.Key([]byte(a.value), userSecrets.Salt, userSecrets.N, userSecrets.R, userSecrets.P, hashLength)
	if err != nil {
		return fmt.Errorf("Error validating while hashing for username=%s", a.key)
	}

	if !reflect.DeepEqual(userSecrets.Hashed, dk) {
		return fmt.Errorf("Invalid credentials")
	}
	return nil
}
