package logger

import (
	"log"

	"go.uber.org/zap"
)

var Logger = setupLogger()

func setupLogger() *zap.SugaredLogger {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("Coulnd't initialize logger: %v", err)
	}
	defer logger.Sync()
	return logger.Sugar()
}
