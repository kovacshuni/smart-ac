package lifecycle

type Shutdownable interface {
	Shutdown() error
}
