package lifecycle

import "net/http"

type ShutdownHandler struct {
	shutdownables []Shutdownable
}

func NewShutdownHandler(shutdownables []Shutdownable) *ShutdownHandler {
	return &ShutdownHandler{
		shutdownables: shutdownables,
	}
}

func (h *ShutdownHandler) Shutdown(w http.ResponseWriter, r *http.Request) {
	for _, shutdownable := range h.shutdownables {
		shutdownable.Shutdown()
	}
}
