package firestoreclient

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/firestore"
)

type FirestoreClient struct {
	Ctx    context.Context
	Client *firestore.Client
}

func NewFirestoreClient(projectID string) *FirestoreClient {
	ctx := context.Background()
	client, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	return &FirestoreClient{
		Ctx:    ctx,
		Client: client,
	}
}

func (c *FirestoreClient) Shutdown() error {
	err := c.Client.Close()
	if err != nil {
		return fmt.Errorf("Error closing firestore database client: %v", err)
	}
	return nil
}
