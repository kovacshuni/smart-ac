package devices

import (
	"fmt"

	"github.com/kovacshuni/smart-ac/firestoreclient"
)

const collectionDevices = "devices"

type firestoreDeviceRegistry struct {
	firestoreClient *firestoreclient.FirestoreClient
}

func NewFirestoreDeviceRegistry(firestoreClient *firestoreclient.FirestoreClient) *firestoreDeviceRegistry {
	return &firestoreDeviceRegistry{
		firestoreClient: firestoreClient,
	}
}

func (r *firestoreDeviceRegistry) List(serialNumbers []string) ([]Device, error) {
	resultDevices := make([]Device, 0)
	if len(serialNumbers) > 0 {
		for _, serialNumber := range serialNumbers {
			device, err := r.Fetch(serialNumber)
			if err != nil {
				return nil, fmt.Errorf("Error fetching device with serialNumber=%s: %v", serialNumber, err)
			}
			if device != nil {
				resultDevices = append(resultDevices, *device)
			}
		}
		return resultDevices, nil
	}

	iter := r.firestoreClient.Client.Collection(collectionDevices).Documents(r.firestoreClient.Ctx)
	dsnaps, err := iter.GetAll()
	if err != nil {
		return nil, fmt.Errorf("Error listing devices: %v", err)
	}
	for _, dsnap := range dsnaps {
		var device Device
		err := dsnap.DataTo(&device)
		if err != nil {
			return nil, fmt.Errorf("Error converting database record to device. record=%v: %v", dsnap, err)
		}
		resultDevices = append(resultDevices, device)
	}
	return resultDevices, nil
}

func (r *firestoreDeviceRegistry) Fetch(serialNumber string) (*Device, error) {
	iter := r.firestoreClient.Client.Collection(collectionDevices).
		Where("serial_number", "==", serialNumber).
		Documents(r.firestoreClient.Ctx)
	dsnaps, err := iter.GetAll()
	if err != nil {
		return nil, fmt.Errorf("Duplicate get devices from iterator for serialNumber=%s", serialNumber)
	}
	if len(dsnaps) > 1 {
		return nil, fmt.Errorf("Duplicate devices for serialNumber=%s", serialNumber)
	}
	if len(dsnaps) == 0 {
		return nil, nil
	}
	var device Device
	err = dsnaps[0].DataTo(&device)
	if err != nil {
		return nil, fmt.Errorf("Error converting database record to device. record=%v: %v", dsnaps[0], err)
	}
	return &device, nil
}

func (r *firestoreDeviceRegistry) Register(device *Device) error {
	_, _, err := r.firestoreClient.Client.Collection(collectionDevices).Add(r.firestoreClient.Ctx, *device)
	if err != nil {
		return fmt.Errorf("Error registering device: %v", err)
	}
	return nil
}
