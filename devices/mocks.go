package devices

import (
	"errors"

	"github.com/stretchr/testify/mock"
)

type mockDeviceRegistry struct {
	mock.Mock
}

func (m *mockDeviceRegistry) Register(device *Device) error {
	args := m.Called(device)
	return args.Error(0)
}

func (m *mockDeviceRegistry) List(serialNumbers []string) ([]Device, error) {
	args := m.Called(serialNumbers)
	return args.Get(0).([]Device), args.Error(1)
}

type errReader struct{}

func (errReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("test error")
}
