package devices

type Device struct {
	SerialNumber    string `json:"serial_number" firestore:"serial_number"`
	FirmwareVersion string `json:"firmware_version" firestore:"firmware_version"`
}

type DevicePage struct {
	Data Device `json:"data"`
}

type DevicesPage struct {
	Data []Device `json:"data"`
}
