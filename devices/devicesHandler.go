package devices

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/kovacshuni/smart-ac/auth"
	"github.com/kovacshuni/smart-ac/errors"
	"github.com/kovacshuni/smart-ac/logger"
)

type DeviceHttpHandler struct {
	deviceRegistry DeviceRegistry
}

func NewDeviceHttpHandler(deviceRegistry DeviceRegistry) *DeviceHttpHandler {
	return &DeviceHttpHandler{
		deviceRegistry: deviceRegistry,
	}
}

func (h *DeviceHttpHandler) RegisterDevice(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/vnd.api+json")

	jsonReqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(fmt.Sprintf("Error reading from request body: %v", err), http.StatusBadRequest, w)
		return
	}
	var device Device
	err = json.Unmarshal(jsonReqBody, &device)
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(fmt.Sprintf("Error unmarshalling JSON device registration request: %v", err), http.StatusBadRequest, w)
		return
	}

	username := r.Header.Get(auth.UsernameHeader)
	if username != device.SerialNumber {
		errors.WriteJsonApiErrorToHTTP(fmt.Sprintf("You do not have permission to register a device other than yourself username=%s serialNumber=%s",
			username, device.SerialNumber), http.StatusUnauthorized, w)
		return
	}

	err = h.deviceRegistry.Register(&device)
	if err != nil {
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}

	marshaledDevicePage, err := json.Marshal(DevicePage{Data: device})
	if err != nil {
		logger.Logger.Warnf("Couldn't marshall registered device to JSON: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(marshaledDevicePage)
	if err != nil {
		logger.Logger.Warnf("Couldn't write registered device serialNumber=%s to HTTP response: %v", device.SerialNumber, err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
	}
}

func (h *DeviceHttpHandler) ListDevices(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/vnd.api+json")

	filterSerialNumber := r.URL.Query()["filter[serial-number]"]
	devices, err := h.deviceRegistry.List(filterSerialNumber)
	if err != nil {
		logger.Logger.Errorf(fmt.Sprintf("Error listing devices with serialNumbers=%v: %v", filterSerialNumber, err))
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}

	if len(devices) == 0 {
		errors.WriteJsonApiErrorToHTTP("No registered devices found for filtered serial numbers.", http.StatusNotFound, w)
		return
	}

	marshaledDevices, err := json.Marshal(DevicesPage{Data: devices})
	if err != nil {
		logger.Logger.Warnf("Couldn't marshall list of devices to JSON: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}

	_, err = w.Write(marshaledDevices)
	if err != nil {
		logger.Logger.Warnf("Couldn't write list of devices to HTTP response: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
}
