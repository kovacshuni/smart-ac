package devices

type DeviceRegistry interface {
	Register(device *Device) error
	List(serialNumbers []string) ([]Device, error)
}
