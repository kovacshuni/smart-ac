package devices

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestDevicesHandler_Register(t *testing.T) {
	serialNumber := "123123456456"
	body := `{"serial_number":"` + serialNumber + `","firmware_version":"1.0.0"}`
	req, err := http.NewRequest("POST", "localhost:8000/devices", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", serialNumber)
	mockedDeviceRegistry := new(mockDeviceRegistry)
	mockedDeviceRegistry.
		On("Register", mock.MatchedBy(func(d *Device) bool { return d.SerialNumber == serialNumber })).
		Return(nil)
	deviceHandler := NewDeviceHttpHandler(mockedDeviceRegistry)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.RegisterDevice)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusCreated, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), body)
}

func TestDevicesHandler_RegistrationBodyCantBeReadError(t *testing.T) {
	req, err := http.NewRequest("POST", "localhost:8000/devices", errReader{})
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	deviceHandler := NewDeviceHttpHandler(nil)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.RegisterDevice)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Error reading from request body:")
}

func TestDevicesHandler_RegistrationBodyNotJsonError(t *testing.T) {
	serialNumber := "123123456456"
	body := `{"serial_number":"` + serialNumber + `","firmware_versi`
	req, err := http.NewRequest("POST", "localhost:8000/devices", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	deviceHandler := NewDeviceHttpHandler(nil)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.RegisterDevice)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Error unmarshalling JSON device registration request")
}

func TestDevicesHandler_RegistrationAuthenticatedNotEqualSNInBody(t *testing.T) {
	serialNumber := "123123456456"
	body := `{"serial_number":"` + serialNumber + `","firmware_version":"1.0.0"}`
	req, err := http.NewRequest("POST", "localhost:8000/devices", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", "someone else")
	deviceHandler := NewDeviceHttpHandler(nil)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.RegisterDevice)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusUnauthorized, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "You do not have permission to register a device other than yourself")
}

func TestDevicesHandler_RegistrationError(t *testing.T) {
	serialNumber := "123123456456"
	body := `{"serial_number":"` + serialNumber + `","firmware_version":"1.0.0"}`
	req, err := http.NewRequest("POST", "localhost:8000/devices", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", serialNumber)
	mockedDeviceRegistry := new(mockDeviceRegistry)
	mockedDeviceRegistry.
		On("Register", mock.MatchedBy(func(d *Device) bool { return d.SerialNumber == serialNumber })).
		Return(fmt.Errorf("reg err"))
	deviceHandler := NewDeviceHttpHandler(mockedDeviceRegistry)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.RegisterDevice)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusInternalServerError, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Internal error, sorry")
}

func TestDevicesHandler_ListOk(t *testing.T) {
	serialNumber := "123123456456"
	req, err := http.NewRequest("GET", "localhost:8000/devices?filter[serial-number]="+serialNumber, nil)
	assert.Nil(t, err)
	req.Header.Add("Accept", "application/json")
	mockedDeviceRegistry := new(mockDeviceRegistry)
	mockedDeviceRegistry.
		On("List", mock.MatchedBy(func(serialNumbers []string) bool { return len(serialNumbers) == 1 && serialNumbers[0] == serialNumber })).
		Return([]Device{
			Device{
				SerialNumber:    serialNumber,
				FirmwareVersion: "1.0.0",
			},
		}, nil)
	deviceHandler := NewDeviceHttpHandler(mockedDeviceRegistry)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.ListDevices)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusOK, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), `{"serial_number":"`+serialNumber+`","firmware_version":"1.0.0"}`)
}

func TestDevicesHandler_ListEmpty(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices", nil)
	assert.Nil(t, err)
	req.Header.Add("Accept", "application/json")
	mockedDeviceRegistry := new(mockDeviceRegistry)
	mockedDeviceRegistry.
		On("List", mock.MatchedBy(func(serialNumbers []string) bool { return len(serialNumbers) == 0 })).
		Return([]Device{}, nil)
	deviceHandler := NewDeviceHttpHandler(mockedDeviceRegistry)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.ListDevices)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusNotFound, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "No registered devices found for filtered serial numbers.")
}

func TestDevicesHandler_ListError(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices", nil)
	assert.Nil(t, err)
	req.Header.Add("Accept", "application/json")
	mockedDeviceRegistry := new(mockDeviceRegistry)
	mockedDeviceRegistry.
		On("List", mock.MatchedBy(func(serialNumbers []string) bool { return len(serialNumbers) == 0 })).
		Return([]Device{}, fmt.Errorf("error listing"))
	deviceHandler := NewDeviceHttpHandler(mockedDeviceRegistry)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(deviceHandler.ListDevices)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusInternalServerError, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Internal error, sorry")
}
