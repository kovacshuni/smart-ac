package httpserver

import (
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	"github.com/kovacshuni/smart-ac/auth"
	"github.com/kovacshuni/smart-ac/devices"
	"github.com/kovacshuni/smart-ac/lifecycle"
	"github.com/kovacshuni/smart-ac/logger"
	"github.com/kovacshuni/smart-ac/metrics"
)

type Routing struct {
	Router *mux.Router
}

func NewRouting(authHandler *auth.AuthHandler,
	deviceHandler *devices.DeviceHttpHandler,
	metricsHandler *metrics.MetricsHttpHandler,
	shutdownHandler *lifecycle.ShutdownHandler) Routing {

	r := Routing{
		Router: mux.NewRouter(),
	}
	r.Router.Path("/health").Handler(handlers.MethodHandler{
		"POST": authHandler.HandleAuth(http.HandlerFunc(healthHandler)),
	})
	r.Router.Path("/devices").Handler(handlers.MethodHandler{
		"POST": authHandler.HandleAuth(http.HandlerFunc(deviceHandler.RegisterDevice)),
		"GET":  http.HandlerFunc(deviceHandler.ListDevices),
	})
	r.Router.Path("/devices/{deviceSerialNumber}/metrics").Handler(handlers.MethodHandler{
		"GET": http.HandlerFunc(metrics.VarsHandler(metricsHandler.ListDeviceMetrics).ServeHTTP),
	})
	r.Router.Path("/metrics").Handler(handlers.MethodHandler{
		"POST": authHandler.HandleAuth(http.HandlerFunc(metricsHandler.AddMetric)),
		"GET":  http.HandlerFunc(metricsHandler.ListMetrics),
	})
	r.Router.Path("/_ah/stop").Handler(handlers.MethodHandler{"POST": http.HandlerFunc(shutdownHandler.Shutdown)})
	return r
}

func (r Routing) ListenAndServe(port string) {
	logger.Logger.Infof("Listening on :%s", port)
	err := http.ListenAndServe(":"+port, r.Router)
	if err != nil {
		logger.Logger.Fatalf("Couldn't serve http endpoints: %v", err)
	}
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}
