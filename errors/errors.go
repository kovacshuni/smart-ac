package errors

import (
	"encoding/json"
	"net/http"

	"github.com/kovacshuni/smart-ac/logger"
)

func WriteJsonApiErrorToHTTP(msg string, status int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/vnd.api+json")
	httpMsg, marshalErr := json.Marshal(NewJsonApiError(msg))
	if marshalErr != nil {
		w.WriteHeader(status)
		return
	}
	w.WriteHeader(status)
	_, err := w.Write(httpMsg)
	if err != nil {
		logger.Logger.Errorf("Couldn't write to http response. %v", err)
	}
}

type JsonApiErrors struct {
	Errors []JsonApiError `json:"errors"`
}

type JsonApiError struct {
	Detail string `json:"detail"`
}

func NewJsonApiError(msg string) JsonApiErrors {
	return JsonApiErrors{Errors: []JsonApiError{JsonApiError{Detail: msg}}}
}
