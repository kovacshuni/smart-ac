# Smart AC Proof of Concept

Part of the solution for Theorem co.’s take-home interview assignement.

It is a preliminary design of the Smart AC server and its integration with clients. 

# Problem statement

One of our clients wants to create a smart air conditioner. They’ve already created the physical device and luckily for us it has full HTTP client functionality.

They want us to do a proof of concept of a backend system which integrates with all of their air conditioning units and provides them with an admin panel to manage their system.

We need to build an HTTP API for the AC units to connect to.

The devices API should support:
* Registering a new device:
  * Each device will have a serial number, a registration date, and a firmware version.
* Allowing each device to authenticate and send data from its sensors once per minute. The sensors on each device provide:
  * Temperature in Celsius.
  * Air humidity percentage.
  * Carbon Monoxide level in the air.
  * Device health status. It can be any text, less than 150 characters.
* Allowing a device to send values for its sensors in bulk, up to 500 values for each of its sensors instead of just 1.
  * If the internet goes out in a home, the AC unit will reconnect when it can to the wifi and send all the data to our servers that it couldn’t send before. After that, it will continue sending data normally, once per minute.
  * Should our servers go down, the devices may retry a few times. Repeat attempts will pack values together until our server goes back up.
* Allowing a client to obtain a list of devices.
  * Being able to filter down by serial number.
* Allowing a client to read device data.
  * Being able to filter down by date.
  * Being able to filter down by sensor type.

Your task consists of
* Creating and deploying this HTTP API for devices, following the requirements outlined above.
* By the end of the exercise, you have to provide us with the URL for the API for devices.
* You also need to provide us with basic documentation for the API for devices.
* You can use any tools or technologies you want.
* You also need to show us the code you used for accomplishing this. This can be a link to a GitHub repo, a zip file, or any way that works best for you.

We know this seems like a lot. The reason we packed so much into the requirements is that this is what real-life situations are sometimes like. It doesn’t mean you need to do everything listed above to pass the exercise though! It just means it is up to you to navigate the situation and figure out how to resolve it.

You need to work with us (the client) to identify what compromises can be made, what things can be put off for later. For us, some requirements are crucial and others are just nice-to-have, and the point of this exercise for you is figuring out which is which, even when we don’t tell you outright.

There are no rules, no predefined criteria for success other than if the client is satisfied at the end of the exercise, you pass. How you accomplish the goal of client happiness is entirely up to you.

We hope you have fun :-) Good luck!

# Solution

## See it working in the cloud

Use the API (documented below) on the URL: `http://[YOUR_PROJECT_ID].appspot.com`, 

__currently `https://smart-ac-255712.appspot.com`__

Example curl requests:

* List devices: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/devices"`
* List devices filterd SNs: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/devices?filter%5Bserial-number%5D=16428362834312&filter%5Bserial-number%5D=218111111111"`
* List device metrics: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/devices/218111111111/metrics"`
* List device metrics newer than datetime: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/devices/218111111111/metrics?filter%5Bstart-date-time%5D=2019-10-19T18:39:01.000Z"`
* List device metrics between datetimes: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/devices/218111111111/metrics?filter%5Bstart-date-time%5D=2019-10-19T18:39:01.000Z&filter%5Bend-date-time%5D=2019-10-19T18:51:59.000Z"`
* List all metrics: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/metrics"`
* List all metrics for multiple SNs: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/metrics?filter%5Bserial-number%5D=16428362834312&filter%5Bserial-number%5D=218111111111"`
* List metrics for multiple SNs and dates: `curl -i -H"Accept:application/vnd.api+json" "https://smart-ac-255712.appspot.com/metrics?filter%5Bserial-number%5D=16428362834312&filter%5Bstart-date-time%5D=2019-10-13T00:00:00.000Z&filter%5Bend-date-time%5D=2019-10-14T23:00:00.000Z"`
* Register device: `curl -i -XPOST -H"Content-Type:application/json" -H"Accept:application/vnd.api+json" -d'{"serial_number": "31313465473","firmware_version": "v1.298 Beta"}' "https://31313465473:pass3@smart-ac-255712.appspot.com/devices"`
* Posting multiple metrics: `curl -i -XPOST -H"Content-Type:application/json" -H"Accept:application/vnd.api+json" -d'[{"air_temperature": 0.2,"air_humidity": 10.9,"air_carbon_monoxide": 0.0,"timestamp": "2019-10-19T10:01:26.000Z"},{"air_temperature": 0.1,"air_humidity": 10.9,"air_carbon_monoxide": 0.0,"timestamp": "2019-10-19T10:01:45.000Z"}]' "https://218111111111:pass1@smart-ac-255712.appspot.com/metrics"`
* Health: `curl -i -XPOST "https://218111111111:pass1@smart-ac-255712.appspot.com/health"`

## How to build and deploy to GCP:

```
gcloud app deploy
```

Logs:
```
gcloud app logs tail -s default
```

## How to build and run locally

You can't run the application locally any more, because its dependency on Google Firestore.

There should be a way, maybe using [firebase emulator](https://firebase.google.com/docs/rules/emulator-setup). Or using an in-memory repository that I used in the beginning of development (can be reverted from git) but that is for later.

Otherwise it would be ran locally like this:

__First time use:__

1. Intall gcloud from [here](https://cloud.google.com/sdk/docs/quickstart-macos) or not sure if brew works `brew install google-cloud-sdk`
1. `gcloud init`
1. sign with the respective google account you have this application deployed on, allow access
1. select project your smart-ac project

__Every time:__

```
dev_appserver.py app.yaml
```

To be able to run `dev_appserver.py app.yaml` your sources must be outside of your GOPATH. I usually keep my working and edited copy in GOPATH but I `git clone $GOPATH/src/github.com/kovacshuni/smart-ac` in another folder and run the AppEngine runtime there. Keep pulling in updates.

## API Design

### Notes

* [JSON:API](https://jsonapi.org) is used for formatting. It is a widely used protocol that supports paging and error messages so it fits best here to use.
* All requests that look like JSON format should contain the Content-Type: applciation/json header.
* All responses contain the Content-Type: application/vnd.api+json header.
* Error responses may contain different texts depending on the case and instance, but their overall structure is the same. One example is given below.

### Registering a device

Authentication credentials needs to be preloaded in user db.

(This might be an issue, maybe registration should include adding an entry to the user db and the credentials provided in the header would be used? Not the nicest way of credentials registration.)

__Request__

HTTP POST /devices

Body:
```
{
  "serial_number": "1938129471",
  "firmware_version": "v1.298 Beta"
}
```

__1. Response:__ On successful registration, the registered record is returned.

HTTP 201 Created

Body:
```
{
  data: {
    "serial_number": "1938129471",
    "firmware_version": "v1.298 Beta"
  }
}
```

__2. Response:__ On missing info

HTTP 400 Bad Request

Body:
```
{
  "errors": [{
    "detail": "Missing field..."
  }]
}
```

Not completing the message text right now but format should be as stated.

### Posting a metric

__Request__
HTTP POST /metric

Body:
```
[
  {
    "air_temperature": 22.4,
    "timestamp": "2019-10-01T12:24:42.231Z"
  },
  {
    "air_humidity": 45.1,
    "air_carbon_monoxide": 0.2,
    "timestamp": "2019-10-01T12:26:42.231Z"
  }
]
```

* Air temperature is measured in Celsius, floating point number
* Air humidity is measured in percentage, floating point number
* Air carbon monoxide level is measured in PPM (parts per million)(definition)
* Timestamp is given by the client, because of records can get old as specified by downtimes on either side/network latencies. It is in format ISO 8601 format, always expressed in UTC timezone like the following: 2019-10-09T13:05:37Z 
* It is the only required field.
* Other fields are optional, but the record will only be saved if there is a metric in the body, otherwise discarded and considered as a health signal. // not implemented yet
* Records can come in an array, in case of a bulk posting because of downtime or other sliding factors specified in the assignement.
* `device_serial_number` is deduced from the Authorization header.

__1. Response:__ On saving a record successfully

HTTP 201 Created

Body:
```
{
  "data": [
    {
      "air_temperature": 22.4,
      "timestamp": "2019-10-01T12:24:42.231Z",
      "device_serial_number": "23979213710937"
    },
    {
      "air_humidity": 45.1,
      "air_carbon_monoxide_level": 0.2,
      "timestamp": "2019-10-01T12:26:42.231Z",
      "device_serial_number": "98151835710275"
    }
  ]
}
```

A records array is returned similar to the request itself.

__2. Response:__ On invalid JSON

HTTP 400 Bad Request with an error message like in previous error cases above.

For the time being, I’m not treating other cases, like timestamp in the future or other invalid-like values.

### Posting a health signal

__Request:__

HTTP POST /health

Headers:

Content-Type: text/plain, or no header at all

Body:
any text up to 150 characters

__1. Response:__ On successful receival of message

HTTP 200 OK with no headers and empty body

I am not sure what is the point of sending health signals to us. The requirements don’t state that we should mark inactive devices stale or how would this benefit the server.

I’m thinking of skipping treating the more than 150 characters long criteria to return a 400. We see how we go with time.

### Listing of devices

__Request:__
HTTP GET /devices?filter[serial-number]={serial-number}

1. Response: 
HTTP 200 OK

Body:
```
{
  "data": [
    {
      "serial_number": "1938129471",
      "firmware_version": "v1.298 Beta"
    }
  ]
}
```

filter param is optional
Square brackets are of course URL encoded but here in the documentation it shows better in raw format.
On too many results some kind of paging should be applied, will have to think about that later.

__2. Response:__ On no device with such serial number

HTTP 404 Not found with empty body

### Read device data by date and sensor type

This endpoint lists one device’s metric datapoints. It is a paginated response meaning that the size of a page is limited (to a predefined number of datapoints) and the client has to parse through the additional pages. Data are sorted by inverse-chronological order.

Cursor is based on date, meaning that each page starts at the given cursor datetime value.

__Request:__
```
HTTP GET /devices/{device-serial-number}/metrics
  &filter[start-date]={start-date-time}
  &filter[end-date-time]={end-date-time}
  &filter[sensor-type]={sensor-type}
  &cursor=2019-10-01T12:24:41.000Z
```

__1. Response:__ On successful find(s)

HTTP 200 OK

Body:
```
{ 
  "data": [
    {
      "air_humidity": 45.1,
      "timestamp": "2019-10-01T12:24:42.231Z",
      "device_serial_number": "12736741687519",
    },
    {
      "air_humidity": 45.2,
      "timestamp": "2019-10-01T12:24:43.200Z",
      "device_serial_number": "28729759759493",
    }
  ]
}
```

* Sorted by timestamp in descending order.
* start-date-time, end-date-time and sensor-type are optional.
* There is no error if start-date is after end-date just a 404.
* In case sensor-type filter is used, records that contain that field will be returned but other metric fields will be stripped. Timestamp and id will be present.
* Paging, again, might be needed in case of lots of data.

__2. Response:__ On no entries satisfying the query

HTTP 404 Not Found

Body:
```
{ 
  "errors": [
    {
      "detail": "No metrics found for registered data and given filters"
    }
  ]
}
```

### Read multiple devices’ data by device, date and sensor type

This endpoint is in addition. It can list multiple devices’ multiple datapoints. It is not implemented to be a paginated response, hence it is unsafe to use with large data.

Data are sorted by inverse-chronological order.

__Request:__

```
HTTP GET /metrics
  ?filter[serial-number]={serial-number}
  &filter[start-date]={start-date-time}
  &filter[end-date-time]={end-date-time}
  &filter[sensor-type]={sensor-type}
```

__1. Response:__ On successful find(s)

HTTP 200 OK

Body:
```
{ 
  "data": [
    {
      "air_humidity": 45.1,
      "timestamp": "2019-10-01T12:24:42.231Z",
      "device_serial_number": "12736741687519",
    },
    {
      "air_humidity": 45.2,
      "timestamp": "2019-10-01T12:24:43.200Z",
      "device_serial_number": "28729759759493",
    }
  ]
}
```

__2. Response:__ On no entries satisfying the query

HTTP 404 Not Found

Body:
```
{"errors": [{"detail": "No metrics found for registered data and given filters"}]}
```

## Authentication

Basic Authentication is implemented over

* posting metrics
* registering devices

No authentication is required for

* listing metrics
* listing devices

in the username:passoword formula the username represents the device’s serial number, in case of devices, or _“admin”_ in case of admin, to be able to differentiate between callers. Every username must have a different authorization key.

Usernames and passwords will be hardcoded for the first iteration. No user database is required.

## Roles

For simlicity let’s consider now that the entire system belongs to one user or one company.

* Listing devices is anyone.
* Any CRUD on a device that is not from the device itself would be admin. (other CRUD is not implemented, than registration, below)
* Devices registration, is a device role.
* Reporting on stats, any users.


## Service architecture and deployment

* Application hosting done using Google Cloud's (GCP) AppEngine.
* For storage, GCP's Firestore is implemented. There are two collections, representing devices and metrics.
* Authentication is included in the Go app, it is just another layer wrapped on top of business handlers.

## Notes

To do:

* In the future, for roles there should be a new layer introduced. Right now role regarding decisions are made in the controller layer. Considering this is changed to a different HTTP or some other interface, the roles businesss should be abstracted.
* Paging right now always shows a next link until the link provided responds with a 404. This could be prevented and next link omitted if all results fitted the current page.
* A version prefix could be introduced to the API e.g. `/v1/devices`
* Cross check when receiveing a posted metric whether the device exists in the registered devices.
