package metrics

import (
	"fmt"
	"time"
)

const timeFormat = "2006-01-02T15:04:05.000Z07:00"

type MetricsMapper struct{}

func (m *MetricsMapper) jsonMetricToMetric(deviceSerialNumber string, jsonMetric JsonMetric) (Metric, error) {
	parsedTime, err := time.Parse(timeFormat, jsonMetric.Timestamp)
	if err != nil {
		return Metric{}, fmt.Errorf("Error parsing timestamp=%s : %v", jsonMetric.Timestamp, err)
	}

	return Metric{
		DeviceSerialNumber: deviceSerialNumber,
		AirTemperature:     jsonMetric.AirTemperature,
		AirHumidity:        jsonMetric.AirHumidity,
		AirCarbonMonoxide:  jsonMetric.AirCarbonMonoxide,
		Timestamp:          parsedTime.In(time.UTC),
	}, nil
}

func (m *MetricsMapper) jsonMetricsToMetrics(deviceSerialNumber string, jsonMetrics []JsonMetric) ([]Metric, error) {
	metrics := make([]Metric, 0)
	for _, jsonMetric := range jsonMetrics {
		metric, err := m.jsonMetricToMetric(deviceSerialNumber, jsonMetric)
		if err != nil {
			return nil, err
		}
		metrics = append(metrics, metric)
	}
	return metrics, nil
}

func (m *MetricsMapper) metricToJsonMetric(metric Metric) JsonMetric {
	return JsonMetric{
		DeviceSerialNumber: metric.DeviceSerialNumber,
		AirTemperature:     metric.AirTemperature,
		AirHumidity:        metric.AirHumidity,
		AirCarbonMonoxide:  metric.AirCarbonMonoxide,
		Timestamp:          metric.Timestamp.Format(timeFormat),
	}
}

func (m *MetricsMapper) metricsToJsonMetrics(metrics []Metric) []JsonMetric {
	jsonMetrics := make([]JsonMetric, 0)
	for _, metric := range metrics {
		jsonMetrics = append(jsonMetrics, m.metricToJsonMetric(metric))
	}
	return jsonMetrics
}
