package metrics

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestMappingJsonToDomain_Ok(t *testing.T) {
	mapper := MetricsMapper{}
	jsonMetric := JsonMetric{
		DeviceSerialNumber: "123",
		AirTemperature:     createFloat64(21.0),
		AirHumidity:        createFloat64(50.0),
		AirCarbonMonoxide:  createFloat64(1.0),
		Timestamp:          "2019-10-18T17:33:01.000Z",
	}
	metric, err := mapper.jsonMetricToMetric("456", jsonMetric)

	expectedMetric := Metric{
		DeviceSerialNumber: "456",
		AirTemperature:     createFloat64(21.0),
		AirHumidity:        createFloat64(50.0),
		AirCarbonMonoxide:  createFloat64(1.0),
		Timestamp:          time.Unix(1571419981, 0).In(time.UTC),
	}

	assert.Nil(t, err)
	assert.Equal(t, expectedMetric, metric)
}

func TestMappingJsonToDomain_ErrorTimestamp(t *testing.T) {
	mapper := MetricsMapper{}
	jsonMetric := JsonMetric{
		DeviceSerialNumber: "123",
		AirTemperature:     createFloat64(21.0),
		AirHumidity:        createFloat64(50.0),
		AirCarbonMonoxide:  createFloat64(1.0),
		Timestamp:          "2019-10-18T17:",
	}
	_, err := mapper.jsonMetricToMetric("456", jsonMetric)

	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "Error parsing timestamp")
}

func TestMappingJsonsToDomains_Ok(t *testing.T) {
	mapper := MetricsMapper{}
	jsonMetrics := []JsonMetric{
		JsonMetric{
			DeviceSerialNumber: "123",
			AirTemperature:     createFloat64(21.0),
			AirHumidity:        createFloat64(50.0),
			AirCarbonMonoxide:  createFloat64(1.0),
			Timestamp:          "2019-10-18T17:33:01.000Z",
		},
	}
	metrics, err := mapper.jsonMetricsToMetrics("456", jsonMetrics)

	expectedMetrics := []Metric{
		Metric{
			DeviceSerialNumber: "456",
			AirTemperature:     createFloat64(21.0),
			AirHumidity:        createFloat64(50.0),
			AirCarbonMonoxide:  createFloat64(1.0),
			Timestamp:          time.Unix(1571419981, 0).In(time.UTC),
		},
	}

	assert.Nil(t, err)
	assert.Equal(t, expectedMetrics, metrics)
}

func TestMappingJsonsToDomains_Error(t *testing.T) {
	mapper := MetricsMapper{}
	jsonMetrics := []JsonMetric{
		JsonMetric{
			DeviceSerialNumber: "123",
			AirTemperature:     createFloat64(21.0),
			AirHumidity:        createFloat64(50.0),
			AirCarbonMonoxide:  createFloat64(1.0),
			Timestamp:          "2019-10-",
		},
	}
	_, err := mapper.jsonMetricsToMetrics("456", jsonMetrics)

	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "Error parsing timestamp")
}

func TestMappingDomainToJson_Ok(t *testing.T) {
	mapper := MetricsMapper{}
	metric := Metric{
		DeviceSerialNumber: "456",
		AirTemperature:     createFloat64(21.0),
		AirHumidity:        createFloat64(50.0),
		AirCarbonMonoxide:  createFloat64(1.0),
		Timestamp:          time.Unix(1571419981, 0).In(time.UTC),
	}
	jsonMetric := mapper.metricToJsonMetric(metric)

	expectedMetric :=
		JsonMetric{
			DeviceSerialNumber: "456",
			AirTemperature:     createFloat64(21.0),
			AirHumidity:        createFloat64(50.0),
			AirCarbonMonoxide:  createFloat64(1.0),
			Timestamp:          "2019-10-18T17:33:01.000Z",
		}

	assert.Equal(t, expectedMetric, jsonMetric)
}

func TestMappingDomainsToJsons_Ok(t *testing.T) {
	mapper := MetricsMapper{}
	metrics := []Metric{
		Metric{
			DeviceSerialNumber: "456",
			AirTemperature:     createFloat64(21.0),
			AirHumidity:        createFloat64(50.0),
			AirCarbonMonoxide:  createFloat64(1.0),
			Timestamp:          time.Unix(1571419981, 0).In(time.UTC),
		},
	}
	jsonMetrics := mapper.metricsToJsonMetrics(metrics)

	expectedMetrics := []JsonMetric{
		JsonMetric{
			DeviceSerialNumber: "456",
			AirTemperature:     createFloat64(21.0),
			AirHumidity:        createFloat64(50.0),
			AirCarbonMonoxide:  createFloat64(1.0),
			Timestamp:          "2019-10-18T17:33:01.000Z",
		},
	}

	assert.Equal(t, expectedMetrics, jsonMetrics)
}
