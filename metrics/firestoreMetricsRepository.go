package metrics

import (
	"fmt"
	"sort"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/kovacshuni/smart-ac/firestoreclient"
)

const collectionMetrics = "metrics"
const pageSize = 3

type firestoreMetricsRepository struct {
	firestoreClient *firestoreclient.FirestoreClient
}

func NewFirestoreMetricsRepository(firestoreClient *firestoreclient.FirestoreClient) *firestoreMetricsRepository {
	return &firestoreMetricsRepository{
		firestoreClient: firestoreClient,
	}
}

func (r *firestoreMetricsRepository) Add(metrics []Metric) error {
	for _, metric := range metrics {
		_, _, err := r.firestoreClient.Client.Collection(collectionMetrics).Add(r.firestoreClient.Ctx, metric)
		if err != nil {
			return fmt.Errorf("Error adding metric: %v", err)
		}
	}
	return nil
}

func (r *firestoreMetricsRepository) ListWithFilter(serialNumbers []string,
	startDateTime *time.Time,
	endDateTime *time.Time,
	sensorTypes []string) ([]*Metric, error) {

	metrics := make([]*Metric, 0)
	if len(serialNumbers) == 0 {
		serialNumbers = append(serialNumbers, "")
	}
	for _, serialNumber := range serialNumbers {
		snMetrics, err := r.ListSNWithFilter(serialNumber, startDateTime, endDateTime, sensorTypes)
		if err != nil {
			return nil, err
		}
		metrics = append(metrics, snMetrics...)
	}
	sort.Sort(byDateDesc(metrics))
	return metrics, nil
}

func (r *firestoreMetricsRepository) ListSNWithFilter(serialNumber string,
	startDateTime *time.Time,
	endDateTime *time.Time,
	sensorTypes []string) ([]*Metric, error) {

	query := r.firestoreClient.Client.Collection(collectionMetrics).Query
	if serialNumber != "" {
		query = query.Where("device_serial_number", "==", serialNumber)
	}
	if startDateTime != nil {
		query = query.Where("timestamp", ">=", startDateTime)
	}
	if endDateTime != nil {
		query = query.Where("timestamp", "<=", endDateTime)
	}
	for _, sensorType := range sensorTypes {
		query = query.Where(sensorType, ">", "")
	}
	query = query.OrderBy("timestamp", firestore.Desc)
	iter := query.Documents(r.firestoreClient.Ctx)
	dsnaps, err := iter.GetAll()
	if err != nil {
		return nil, fmt.Errorf("Error getting results from iterator for serialNumber=%s startDateTime=%s endDateTime=%s sensorTypes=%v: %v",
			serialNumber, startDateTime.String(), endDateTime.String(), sensorTypes, err)
	}
	resultMetrics := make([]*Metric, 0)
	for _, dsnap := range dsnaps {
		var m Metric
		err = dsnap.DataTo(&m)
		if err != nil {
			return nil, fmt.Errorf("Error converting database record to metric. record=%v: %v", dsnap, err)
		}
		resultMetrics = append(resultMetrics, &m)
	}
	return resultMetrics, nil
}

func (r *firestoreMetricsRepository) ListSNPaging(serialNumber string,
	startDateTime *time.Time,
	endDateTime *time.Time,
	sensorTypes []string,
	cursorTime *time.Time) ([]*Metric, *time.Time, error) {

	query := r.firestoreClient.Client.Collection(collectionMetrics).Query
	if serialNumber != "" {
		query = query.Where("device_serial_number", "==", serialNumber)
	}
	if startDateTime != nil {
		query = query.Where("timestamp", ">=", startDateTime)
	}
	if endDateTime != nil {
		query = query.Where("timestamp", "<=", endDateTime)
	}
	for _, sensorType := range sensorTypes {
		query = query.Where(sensorType, ">", "")
	}
	query = query.OrderBy("timestamp", firestore.Desc)
	if cursorTime != nil {
		query = query.StartAfter(cursorTime)
	}
	query = query.Limit(pageSize)

	iter := query.Documents(r.firestoreClient.Ctx)

	dsnaps, err := iter.GetAll()

	if err != nil {
		return nil, nil, fmt.Errorf("Error getting results from iterator for serialNumber=%s startDateTime=%s endDateTime=%s sensorTypes=%v: %v",
			serialNumber, startDateTime.String(), endDateTime.String(), sensorTypes, err)
	}

	resultMetrics := make([]*Metric, 0)
	var lastTimestamp time.Time
	for _, dsnap := range dsnaps {
		var m Metric
		err = dsnap.DataTo(&m)
		if err != nil {
			return nil, nil, fmt.Errorf("Error converting database record to metric. record=%v: %v", dsnap, err)
		}
		resultMetrics = append(resultMetrics, &m)
		lastTimestamp = m.Timestamp
	}

	return resultMetrics, &lastTimestamp, nil
}
