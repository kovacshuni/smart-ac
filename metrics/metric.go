package metrics

import "time"

type JsonMetric struct {
	DeviceSerialNumber string   `json:"device_serial_number"`
	AirTemperature     *float64 `json:"air_temperature,omitempty"`
	AirHumidity        *float64 `json:"air_humidity,omitempty"`
	AirCarbonMonoxide  *float64 `json:"air_carbon_monoxide,omitempty"`
	Timestamp          string   `json:"timestamp"`
}

type Links struct {
	Next string `json:"next,omitempty"`
}

type JsonMetricsPage struct {
	Data  []JsonMetric `json:"data"`
	Links *Links       `json:"links,omitempty"`
}

// Exported for cloud.google Firestore library to be able to access
type Metric struct {
	DeviceSerialNumber string    `firestore:"device_serial_number"`
	AirTemperature     *float64  `firestore:"air_temperature,omitempty"`
	AirHumidity        *float64  `firestore:"air_humidity,omitempty"`
	AirCarbonMonoxide  *float64  `firestore:"air_carbon_monoxide,omitempty"`
	Timestamp          time.Time `firestore:"timestamp"`
}

func createFloat64(x float64) *float64 {
	return &x
}

type byDateDesc []*Metric

func (d byDateDesc) Len() int {
	return len(d)
}
func (d byDateDesc) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}
func (d byDateDesc) Less(i, j int) bool {
	return d[i].Timestamp.After(d[j].Timestamp)
}
