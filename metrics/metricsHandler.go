package metrics

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/kovacshuni/smart-ac/auth"
	"github.com/kovacshuni/smart-ac/errors"
	"github.com/kovacshuni/smart-ac/logger"
)

type MetricsHttpHandler struct {
	metricsRepository MetricsRepository
	metricsMapper     *MetricsMapper
}

func NewMetricsHttpHandler(metricsRepository MetricsRepository, metricsMapper *MetricsMapper) *MetricsHttpHandler {
	return &MetricsHttpHandler{
		metricsRepository: metricsRepository,
		metricsMapper:     metricsMapper,
	}
}

type VarsHandler func(w http.ResponseWriter, r *http.Request, vars map[string]string)

func (vh VarsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	vh(w, r, vars)
}

func (h *MetricsHttpHandler) AddMetric(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/vnd.api+json")

	jsonReqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(fmt.Sprintf("Error reading from request body: %v", err), http.StatusBadRequest, w)
		return
	}
	var jsonMetrics []JsonMetric
	err = json.Unmarshal(jsonReqBody, &jsonMetrics)
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(fmt.Sprintf("Error unmarshalling JSON metric request: %v", err), http.StatusBadRequest, w)
		return
	}

	username := r.Header.Get(auth.UsernameHeader)
	if username == "admin" {
		errors.WriteJsonApiErrorToHTTP("Admins shouldn't post metrics.", http.StatusUnauthorized, w)
		return
	}
	metrics, err := h.metricsMapper.jsonMetricsToMetrics(username, jsonMetrics)
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(fmt.Sprintf("Error validating metric: %v", err), http.StatusBadRequest, w)
		return
	}

	err = h.metricsRepository.Add(metrics)
	if err != nil {
		logger.Logger.Errorf("Error adding metric to repository: %v", err.Error())
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}

	returnedMetric := h.metricsMapper.metricsToJsonMetrics(metrics)

	marshaledMetrics, err := json.Marshal(JsonMetricsPage{Data: returnedMetric})
	if err != nil {
		logger.Logger.Warnf("Couldn't marshall added metric to JSON: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(marshaledMetrics)
	if err != nil {
		logger.Logger.Warnf("Couldn't write added metric to HTTP response: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
}

func (h *MetricsHttpHandler) ListDeviceMetrics(w http.ResponseWriter, r *http.Request, vars map[string]string) {
	w.Header().Add("Content-Type", "application/vnd.api+json")

	deviceSerialNumber := vars["deviceSerialNumber"]
	filterStartDateTime, err := h.parseDateTime(r.URL, "filter[start-date-time]")
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(err.Error(), http.StatusBadRequest, w)
		return
	}
	filterEndDateTime, err := h.parseDateTime(r.URL, "filter[end-date-time]")
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(err.Error(), http.StatusBadRequest, w)
		return
	}
	filterSensorTypes := r.URL.Query()["filter[sensor-type]"]
	cursor, err := h.parseDateTime(r.URL, "cursor")
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(err.Error(), http.StatusBadRequest, w)
		return
	}

	filteredMetrics, nextCursor, err := h.metricsRepository.ListSNPaging(deviceSerialNumber, filterStartDateTime,
		filterEndDateTime, filterSensorTypes, cursor)
	if err != nil {
		if strings.Contains(err.Error(), "Bad cursor") {
			errors.WriteJsonApiErrorToHTTP("Bad cursor", http.StatusBadRequest, w)
			return
		}
		logger.Logger.Errorf("Couldn't find filtered metrics: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
	if len(filteredMetrics) == 0 {
		errors.WriteJsonApiErrorToHTTP("No metrics found for registered data and given filters.", http.StatusNotFound, w)
		return
	}

	jsonMetrics := make([]JsonMetric, 0)
	for _, filteredMetric := range filteredMetrics {
		jsonMetrics = append(jsonMetrics, h.metricsMapper.metricToJsonMetric(*filteredMetric))
	}

	nextUrl, err := h.createUrlWithNewCursor(nextCursor, r.URL)
	if err != nil {
		logger.Logger.Error(err.Error())
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
	page := JsonMetricsPage{
		Data: jsonMetrics,
		Links: &Links{
			Next: nextUrl.String(),
		},
	}
	marshaledPage, err := json.Marshal(page)
	if err != nil {
		logger.Logger.Errorf("Couldn't marshall filtered metrics to JSON: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
	_, err = w.Write(marshaledPage)
	if err != nil {
		logger.Logger.Errorf("Couldn't write filtered metrics to HTTP response: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
}

func (h *MetricsHttpHandler) parseDateTime(url *url.URL, paramName string) (*time.Time, error) {
	filterDateTimes := url.Query()[paramName]
	var filterDateTime *time.Time
	if len(filterDateTimes) == 1 && filterDateTimes[0] != "" {
		parsed, err := time.Parse(timeFormat, filterDateTimes[0])
		if err != nil {
			return nil, fmt.Errorf("Error parsing query parameter %s=%s: %v", paramName, filterDateTimes[0], err)
		}
		filterDateTime = &parsed
	}
	return filterDateTime, nil
}

func (h *MetricsHttpHandler) createUrlWithNewCursor(nextCursor *time.Time, theUrl *url.URL) (*url.URL, error) {
	nextCursorStr := nextCursor.Format(timeFormat)
	nextUrl, err := url.Parse(theUrl.String())
	if err != nil {
		return nil, fmt.Errorf("Couldn't parse request URL=%s: %v", theUrl.String(), err)
	}
	nextQuery := nextUrl.Query()
	nextQuery.Set("cursor", nextCursorStr)
	nextUrl.RawQuery = nextQuery.Encode()
	return nextUrl, nil
}

func (h *MetricsHttpHandler) ListMetrics(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json;charset=utf-8")

	filterSerialNumber := r.URL.Query()["filter[serial-number]"]
	filterStartDateTime, err := h.parseDateTime(r.URL, "filter[start-date-time]")
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(err.Error(), http.StatusBadRequest, w)
		return
	}
	filterEndDateTime, err := h.parseDateTime(r.URL, "filter[end-date-time]")
	if err != nil {
		errors.WriteJsonApiErrorToHTTP(err.Error(), http.StatusBadRequest, w)
		return
	}
	filterSensorTypes := r.URL.Query()["filter[sensor-types]"]

	filteredMetrics, err := h.metricsRepository.ListWithFilter(filterSerialNumber, filterStartDateTime, filterEndDateTime, filterSensorTypes)
	if err != nil {
		logger.Logger.Errorf("Couldn't find filtered metrics: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}

	if len(filteredMetrics) == 0 {
		errors.WriteJsonApiErrorToHTTP("No metrics found for registered data and given filters.", http.StatusNotFound, w)
		return
	}
	jsonMetrics := make([]JsonMetric, 0)
	for _, filteredMetric := range filteredMetrics {
		jsonMetrics = append(jsonMetrics, h.metricsMapper.metricToJsonMetric(*filteredMetric))
	}
	marshaledMetrics, err := json.Marshal(JsonMetricsPage{Data: jsonMetrics})
	if err != nil {
		logger.Logger.Errorf("Couldn't marshall filtered metrics to JSON: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
	_, err = w.Write(marshaledMetrics)
	if err != nil {
		logger.Logger.Errorf("Couldn't write filtered metrics to HTTP response: %v", err)
		errors.WriteJsonApiErrorToHTTP("Internal error, sorry", http.StatusInternalServerError, w)
		return
	}
}
