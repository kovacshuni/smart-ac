package metrics

import (
	"time"

	"github.com/stretchr/testify/mock"
)

type mockMetricsRepository struct {
	mock.Mock
}

func (m *mockMetricsRepository) Add(metrics []Metric) error {
	args := m.Called(metrics)
	if args.Get(0) == nil {
		return nil
	}
	return args.Error(0)
}

func (m *mockMetricsRepository) ListWithFilter(serialNumbers []string,
	startDateTime *time.Time,
	endDateTime *time.Time,
	sensorTypes []string) ([]*Metric, error) {

	args := m.Called(serialNumbers, startDateTime, endDateTime, sensorTypes)
	if args.Get(1) != nil {
		return nil, args.Error(1)
	}
	return (args.Get(0)).([]*Metric), nil
}

func (m *mockMetricsRepository) ListSNPaging(serialNumber string,
	startDateTime *time.Time,
	endDateTime *time.Time,
	sensorTypes []string,
	cursorTime *time.Time) ([]*Metric, *time.Time, error) {

	args := m.Called(serialNumber, startDateTime, endDateTime, sensorTypes, cursorTime)
	if args.Get(2) != nil {
		return nil, nil, args.Error(2)
	}
	return (args.Get(0)).([]*Metric), args.Get(1).(*time.Time), nil
}
