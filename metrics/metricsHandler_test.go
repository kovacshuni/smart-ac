package metrics

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestMetricsHandlerList_Ok(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/metrics?filter%5Bserial-number%5D=4108704012479&filter%5Bserial-number%5D=8327428374239&filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&filter%5Bend-date-time%5D=2019-10-19T08:57:26.000Z", nil)
	assert.Nil(t, err)

	testMetricSamples := []*Metric{
		&Metric{
			DeviceSerialNumber: "4108704012479",
			AirTemperature:     createFloat64(23.1),
			AirHumidity:        createFloat64(50),
			AirCarbonMonoxide:  createFloat64(0.18),
			Timestamp:          time.Unix(1571475363, 0).In(time.UTC),
		},
		&Metric{
			DeviceSerialNumber: "4108704012479",
			AirTemperature:     createFloat64(25.1),
			AirHumidity:        createFloat64(45),
			AirCarbonMonoxide:  createFloat64(0.18),
			Timestamp:          time.Unix(1571475426, 0).In(time.UTC),
		},
		&Metric{
			DeviceSerialNumber: "8327428374239",
			AirTemperature:     createFloat64(17.5),
			AirHumidity:        createFloat64(43.3),
			AirCarbonMonoxide:  createFloat64(0.19),
			Timestamp:          time.Unix(1571475445, 0).In(time.UTC),
		},
	}
	mockedMetricsRepository := new(mockMetricsRepository)
	var nilStrings []string
	mockedMetricsRepository.
		On("ListWithFilter", []string{"4108704012479", "8327428374239"},
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475362 }),
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475446 }),
			nilStrings).
		Return(testMetricSamples, nil)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)

	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.ListMetrics)

	handler.ServeHTTP(reqRecorder, req)

	if reqRecorder.Code != http.StatusOK {
		t.Errorf("handler returned wrong statusCode=%d expected=%d", reqRecorder.Code, http.StatusOK)
	}
	expectedRecords := []string{`{"device_serial_number":"4108704012479","air_temperature":23.1,"air_humidity":50,"air_carbon_monoxide":0.18,"timestamp":"2019-10-19T08:56:03.000Z"}`,
		`{"device_serial_number":"4108704012479","air_temperature":25.1,"air_humidity":45,"air_carbon_monoxide":0.18,"timestamp":"2019-10-19T08:57:06.000Z"}`,
		`{"device_serial_number":"8327428374239","air_temperature":17.5,"air_humidity":43.3,"air_carbon_monoxide":0.19,"timestamp":"2019-10-19T08:57:25.000Z"}`}
	for _, expectedRecord := range expectedRecords {
		assert.Contains(t, reqRecorder.Body.String(), expectedRecord)
	}
}

func TestMetricsHandlerList_EmptyGives404(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/metrics", nil)
	assert.Nil(t, err)
	mockedMetricsRepository := new(mockMetricsRepository)
	var nilTime *time.Time
	var nilStrings []string
	mockedMetricsRepository.On("ListWithFilter",
		mock.MatchedBy(func(serialNumbers []string) bool { return len(serialNumbers) == 0 }),
		nilTime, nilTime, nilStrings).Return([]*Metric{}, nil)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.ListMetrics)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusNotFound, reqRecorder.Code)
	assert.Equal(t, reqRecorder.Body.String(), `{"errors":[{"detail":"No metrics found for registered data and given filters."}]}`)
}

func TestMetricsHandlerList_ErrorStartDate(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/metrics?filter%5Bstart-date-time%5D=2019-10-11T10:5", nil)
	assert.Nil(t, err)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.ListMetrics)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Contains(t, reqRecorder.Body.String(), "Error parsing query parameter filter[start-date-time]")
}

func TestMetricsHandlerList_ErrorEndDate(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/metrics?filter%5Bend-date-time%5D=2019-10-11T10:5", nil)
	assert.Nil(t, err)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.ListMetrics)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Contains(t, reqRecorder.Body.String(), "Error parsing query parameter filter[end-date-time]")
}

func TestMetricsHandlerList_InternalError(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/metrics", nil)
	assert.Nil(t, err)
	mockedMetricsRepository := new(mockMetricsRepository)
	var nilTime *time.Time
	var nilStrings []string
	mockedMetricsRepository.
		On("ListWithFilter", nilStrings, nilTime, nilTime, nilStrings).
		Return(nil, fmt.Errorf("couldn't read db"))
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.ListMetrics)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusInternalServerError, reqRecorder.Code)
	assert.Contains(t, reqRecorder.Body.String(), "Internal error, sorry")
}

const myiota = iota

func TestMetricsHandlerListPaging_Ok(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&"+
		"filter%5Bend-date-time%5D=2019-10-19T08:57:26.000Z&"+
		"filter%5Bsensor-type%5D=air_temperature&"+
		"cursor=2019-10-19T08:56:01.000Z", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	testMetricSamples := []*Metric{
		&Metric{
			DeviceSerialNumber: "4108704012479",
			AirTemperature:     createFloat64(23.1),
			AirHumidity:        createFloat64(50),
			AirCarbonMonoxide:  createFloat64(0.18),
			Timestamp:          time.Unix(1571475363, 0).In(time.UTC),
		},
		&Metric{
			DeviceSerialNumber: "4108704012479",
			AirTemperature:     createFloat64(25.1),
			AirHumidity:        createFloat64(45),
			AirCarbonMonoxide:  createFloat64(0.18),
			Timestamp:          time.Unix(1571475426, 0).In(time.UTC),
		},
	}
	nextCursor := time.Unix(1571475426, 0).In(time.UTC) //"2019-10-19T08:57:26.000Z"
	mockedMetricsRepository := new(mockMetricsRepository)
	mockedMetricsRepository.
		On("ListSNPaging",
			"4108704012479",
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475362 }),
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475446 }),
			[]string{"air_temperature"},
			mock.MatchedBy(func(t *time.Time) bool { return true })).
		Return(testMetricSamples, &nextCursor, nil)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusOK, reqRecorder.Code)
	expectedBody := `{"data":[` +
		`{"device_serial_number":"4108704012479","air_temperature":23.1,"air_humidity":50,"air_carbon_monoxide":0.18,"timestamp":"2019-10-19T08:56:03.000Z"},` +
		`{"device_serial_number":"4108704012479","air_temperature":25.1,"air_humidity":45,"air_carbon_monoxide":0.18,"timestamp":"2019-10-19T08:57:06.000Z"}` +
		`],` +
		`"links":{"next":"localhost:8000/devices/4108704012479/metrics?cursor=2019-10-19T08%3A57%3A06.000Z\u0026filter%5Bend-date-time%5D=2019-10-19T08%3A57%3A26.000Z\u0026filter%5Bsensor-type%5D=air_temperature\u0026filter%5Bstart-date-time%5D=2019-10-19T08%3A56%3A02.000Z"}}`
	assert.Equal(t, expectedBody, reqRecorder.Body.String())
}

func TestMetricsHandlerListPaging_ErrorStartDate(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-80-19T08:56:02.000Z", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Contains(t, reqRecorder.Body.String(), `{"errors":[{"detail":"Error parsing query parameter filter[start-date-time]`)
}

func TestMetricsHandlerListPaging_ErrorEndDate(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&"+
		"filter%5Bend-date-time%5D=2019-10-", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Contains(t, reqRecorder.Body.String(), `{"errors":[{"detail":"Error parsing query parameter filter[end-date-time]`)
}

func TestMetricsHandlerListPaging_ErrorCursor(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&"+
		"filter%5Bend-date-time%5D=2019-10-19T08:57:26.000Z&"+
		"cursor=2019-10-19T08:56:0", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Contains(t, reqRecorder.Body.String(), `{"errors":[{"detail":"Error parsing query parameter cursor`)
}

func TestMetricsHandlerListPaging_ErrorListing(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&"+
		"filter%5Bend-date-time%5D=2019-10-19T08:57:26.000Z&"+
		"filter%5Bsensor-type%5D=air_temperature&"+
		"cursor=2019-10-19T08:56:01.000Z", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	mockedMetricsRepository := new(mockMetricsRepository)
	mockedMetricsRepository.
		On("ListSNPaging",
			"4108704012479",
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475362 }),
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475446 }),
			[]string{"air_temperature"},
			mock.MatchedBy(func(t *time.Time) bool { return true })).
		Return(nil, nil, fmt.Errorf("error listing"))
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusInternalServerError, reqRecorder.Code)
	assert.Equal(t, `{"errors":[{"detail":"Internal error, sorry"}]}`, reqRecorder.Body.String())
}

func TestMetricsHandlerListPaging_ErrorWithCursor(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&"+
		"filter%5Bend-date-time%5D=2019-10-19T08:57:26.000Z&"+
		"filter%5Bsensor-type%5D=air_temperature&"+
		"cursor=2019-10-19T08:56:01.000Z", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	mockedMetricsRepository := new(mockMetricsRepository)
	mockedMetricsRepository.
		On("ListSNPaging",
			"4108704012479",
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475362 }),
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475446 }),
			[]string{"air_temperature"},
			mock.MatchedBy(func(t *time.Time) bool { return true })).
		Return(nil, nil, fmt.Errorf("Bad cursor, outside of range"))
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Code)
	assert.Equal(t, `{"errors":[{"detail":"Bad cursor"}]}`, reqRecorder.Body.String())
}

func TestMetricsHandlerListPaging_Empty(t *testing.T) {
	req, err := http.NewRequest("GET", "localhost:8000/devices/4108704012479/metrics?"+
		"filter%5Bstart-date-time%5D=2019-10-19T08:56:02.000Z&"+
		"filter%5Bend-date-time%5D=2019-10-19T08:57:26.000Z&"+
		"filter%5Bsensor-type%5D=air_temperature&"+
		"cursor=2019-10-19T08:56:01.000Z", nil)
	assert.Nil(t, err)
	vars := map[string]string{"deviceSerialNumber": "4108704012479"}
	mockedMetricsRepository := new(mockMetricsRepository)
	var nilTime *time.Time
	mockedMetricsRepository.
		On("ListSNPaging",
			"4108704012479",
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475362 }),
			mock.MatchedBy(func(t *time.Time) bool { return t.Unix() == 1571475446 }),
			[]string{"air_temperature"},
			mock.MatchedBy(func(t *time.Time) bool { return true })).
		Return([]*Metric{}, nilTime, nil)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()

	metricsHandler.ListDeviceMetrics(reqRecorder, req, vars)

	assert.Equal(t, http.StatusNotFound, reqRecorder.Code)
	assert.Equal(t, `{"errors":[{"detail":"No metrics found for registered data and given filters."}]}`, reqRecorder.Body.String())
}

func TestMetricsHandlerAdd_Ok(t *testing.T) {
	serialNumber := "123123456456"
	body := `[{"air_temperature":21.9,"timestamp":"2019-10-18T17:33:01.000Z"},` +
		`{"air_humidity":67.8,"timestamp":"2019-10-18T17:33:00.000Z"}]`
	req, err := http.NewRequest("POST", "localhost:8000/metrics", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", serialNumber)
	mockedMetricsRepository := new(mockMetricsRepository)
	mockedMetricsRepository.
		On("Add", mock.MatchedBy(func(metric []Metric) bool { return true })).
		Return(nil)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.AddMetric)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusCreated, reqRecorder.Result().StatusCode)
	expectedRespBody := `{"data":[{"device_serial_number":"` + serialNumber + `","air_temperature":21.9,"timestamp":"2019-10-18T17:33:01.000Z"},` +
		`{"device_serial_number":"` + serialNumber + `","air_humidity":67.8,"timestamp":"2019-10-18T17:33:00.000Z"}]}`
	assert.Equal(t, expectedRespBody, reqRecorder.Body.String())
}

func TestMetricsHandlerAdd_ErrorReadingRequest(t *testing.T) {
	req, err := http.NewRequest("POST", "localhost:8000/metrics", errReader{})
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.AddMetric)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Error reading from request body")
}

func TestMetricsHandlerAdd_ErrorParsingJson(t *testing.T) {
	body := `[{"air_temperature":21.9,"timesta`
	req, err := http.NewRequest("POST", "localhost:8000/metrics", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.AddMetric)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Error unmarshalling JSON metric request")
}

func TestMetricsHandlerAdd_AdminShouldntCreate(t *testing.T) {
	body := `[{"air_temperature":21.9,"timestamp":"2019-10-18T17:33:01.000Z"}]`
	req, err := http.NewRequest("POST", "localhost:8000/metrics", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", "admin")
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.AddMetric)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusUnauthorized, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Admins shouldn't post metrics.")
}

func TestMetricsHandlerAdd_ErrorMapping(t *testing.T) {
	serialNumber := "123123456456"
	body := `[{"air_temperature":21.9,"timestamp":"2019"}]`
	req, err := http.NewRequest("POST", "localhost:8000/metrics", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", serialNumber)
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(nil, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.AddMetric)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusBadRequest, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Error validating metric")
}

func TestMetricsHandlerAdd_ErrorAdding(t *testing.T) {
	serialNumber := "123123456456"
	body := `[{"air_temperature":21.9,"timestamp":"2019-10-18T17:33:01.000Z"}]`
	req, err := http.NewRequest("POST", "localhost:8000/metrics", strings.NewReader(body))
	assert.Nil(t, err)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-Username", serialNumber)
	mockedMetricsRepository := new(mockMetricsRepository)
	mockedMetricsRepository.
		On("Add", mock.MatchedBy(func(metrics []Metric) bool { return true })).
		Return(fmt.Errorf("error adding"))
	metricsMapper := MetricsMapper{}
	metricsHandler := NewMetricsHttpHandler(mockedMetricsRepository, &metricsMapper)
	reqRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(metricsHandler.AddMetric)

	handler.ServeHTTP(reqRecorder, req)

	assert.Equal(t, http.StatusInternalServerError, reqRecorder.Result().StatusCode)
	assert.Contains(t, reqRecorder.Body.String(), "Internal error, sorry")
}

type errReader struct{}

func (errReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("test error")
}
