package metrics

import (
	"time"
)

type MetricsRepository interface {
	Add(metrics []Metric) error
	ListWithFilter(serialNumbers []string,
		startDateTime *time.Time,
		endDateTime *time.Time,
		sensorTypes []string) ([]*Metric, error)
	ListSNPaging(serialNumber string,
		startDateTime *time.Time,
		endDateTime *time.Time,
		sensorTypes []string,
		cursorTime *time.Time) ([]*Metric, *time.Time, error)
}
