package main

import (
	"net/http"

	"github.com/kovacshuni/smart-ac/auth"
	"github.com/kovacshuni/smart-ac/devices"
	"github.com/kovacshuni/smart-ac/firestoreclient"
	"github.com/kovacshuni/smart-ac/httpserver"
	"github.com/kovacshuni/smart-ac/lifecycle"
	"github.com/kovacshuni/smart-ac/metrics"
	"google.golang.org/appengine"
)

const projectID = "smart-ac-255712"

func main() {
	// should never be hardcoded, but this is a prototype
	userDatabase := map[string]auth.Secrets{
		"admin": auth.Secrets{ // password
			Salt:   []byte{18, 212, 230, 133, 157, 163, 138, 86, 24, 89, 32, 246, 146, 109, 1, 251},
			N:      32768,
			R:      8,
			P:      1,
			Hashed: []byte{30, 228, 27, 53, 155, 103, 24, 190, 33, 184, 103, 143, 211, 195, 28, 68, 40, 65, 205, 159, 66, 110, 189, 81, 11, 35, 190, 81, 136, 146, 199, 104},
		},
		"218111111111": auth.Secrets{ // pass1
			Salt:   []byte{61, 27, 101, 90, 80, 246, 19, 128, 99, 88, 27, 172, 40, 216, 88, 20},
			N:      32768,
			R:      8,
			P:      1,
			Hashed: []byte{249, 58, 65, 182, 160, 125, 44, 10, 215, 119, 232, 151, 114, 108, 226, 145, 220, 166, 31, 6, 249, 13, 32, 162, 4, 69, 163, 150, 127, 194, 24, 233},
		},
		"16428362834312": auth.Secrets{ // pass2
			Salt:   []byte{85, 84, 146, 236, 155, 36, 70, 60, 233, 239, 129, 161, 62, 211, 235, 28},
			N:      32768,
			R:      8,
			P:      1,
			Hashed: []byte{50, 60, 39, 187, 11, 158, 185, 157, 19, 229, 246, 45, 98, 206, 28, 254, 107, 146, 117, 74, 27, 78, 249, 93, 71, 173, 149, 45, 112, 169, 122, 140},
		},
		"31313465473": auth.Secrets{ // pass3
			Salt:   []byte{241, 189, 122, 0, 88, 144, 248, 108, 193, 54, 74, 48, 178, 22, 175, 36},
			N:      32768,
			R:      8,
			P:      1,
			Hashed: []byte{129, 247, 0, 151, 198, 217, 214, 46, 87, 252, 253, 5, 250, 204, 29, 159, 160, 181, 177, 187, 22, 206, 21, 21, 216, 80, 88, 111, 165, 152, 232, 222},
		},
	}

	firestoreClient := firestoreclient.NewFirestoreClient(projectID)
	deviceRegistry := devices.NewFirestoreDeviceRegistry(firestoreClient)
	deviceHandler := devices.NewDeviceHttpHandler(deviceRegistry)
	metricsRepository := metrics.NewFirestoreMetricsRepository(firestoreClient)
	metricsMapper := metrics.MetricsMapper{}
	metricsHandler := metrics.NewMetricsHttpHandler(metricsRepository, &metricsMapper)
	shutdownHandler := lifecycle.NewShutdownHandler([]lifecycle.Shutdownable{firestoreClient})
	authValidator := auth.NewPasswordValidator(userDatabase)
	authHandler := auth.NewAuthHandler(authValidator)
	routing := httpserver.NewRouting(authHandler, deviceHandler, metricsHandler, shutdownHandler)

	http.Handle("/", routing.Router)
	appengine.Main()
}
